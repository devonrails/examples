import io
import json
import os
import sys
import time

from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer


def get_bootstrap_server(node_index):
    """
    Return a bootstrap server based on the index of the node in the cluster.
    """
    hostname_suffix = 'worker-green-village.green-village.sda-projects.nl'
    return f'{node_index + 1:02}-{hostname_suffix}:{31090 + node_index}'


def delivery_report(err, msg):
    """
    Called once for each message produced to indicate delivery result.
    Triggered by poll() or flush().
    """
    if err:
        print(f'Message delivery failed: {err}')
    else:
        print(f'Message delivered to {msg.topic()} [{msg.partition()}] at offset {msg.offset()}')


def get_schema(path):
    """
    Return Avro schema from path.
    """
    with open(path) as stream:
        return avro.loads(stream.read())


def main():
    num_cluster_nodes = 3  # change this number to the number of nodes (brokers) in your cluster
    bootstrap_servers = [get_bootstrap_server(i) for i in range(num_cluster_nodes)]

    # Set up producer with bootstrap servers pointing to the cluster nodes, as well
    # as the username and password and other security configuration values.

    producer = AvroProducer({
        "bootstrap.servers": ','.join(bootstrap_servers),
        'schema.registry.url': 'https://schema-registry.green-village.sda-projects.nl:443/',
        "security.protocol": "SASL_PLAINTEXT",
        "sasl.mechanisms": "PLAIN",
        "sasl.username": "test",
        "sasl.password": "1234"
    })

    # Get Avro schema

    schema = get_schema('schema.avsc')

    message = {
        "data": {
            "timestamp": int(time.time() * 1000),
            "project_id": "test",
            "device_id": "device1",
            "values": [
                {
                    "description": "temperature",
                    "name": "temperature",
                    "type": "INT",
                    "unit": "degrees",
                    "value": {
                        "int": 42
                    }
                }
            ]
        },
        "metadata": {
            "description": "test device",
            "location": None,
            "latitude": None,
            "longitude": None,
            "altitude": None,
            "type": None,
            "manufacturer": None,
            "placement_timestamp": None,
            "project_id": "test",
            "device_id": "device1",
            "serial": None
        }
    }

    try:
        while True:
            # Asynchronously produce a message, the delivery report callback
            # will be triggered from poll() above, or flush() below, when the message has
            # been successfully delivered or failed permanently.

            producer.produce(topic='test-schema', value=message, value_schema=schema, callback=delivery_report)

            # Trigger any available delivery report callbacks from previous produce() call

            producer.poll(0)

            # Sleep for a bit so we don't flood the cluster

            time.sleep(1)
    except KeyboardInterrupt:
        print('Aborted by user', file=sys.stderr)
    finally:
        # Wait for any outstanding messages to be delivered and delivery report
        # callbacks to be triggered.

        producer.flush()


# Entry point

if __name__ == '__main__':
    main()
