# Kafka producer/consumer examples
This repository contains a number of examples for producing to and consuming from Kafka. There are four ways of producing and consuming: with the [Kafka REST Proxy](https://docs.confluent.io/current/kafka-rest/index.html), with the [confluent-kafka-python package](https://github.com/confluentinc/confluent-kafka-python), with the Kafka console producers and consumers, and with [kafkacat](https://github.com/edenhill/kafkacat).

Each project has a dedicated topic with the name `${PROJECT_NAME}-schema`. This topic requires data formatted in compliance with the Avro schema. **Important note: it is still possible to produce messages to the topic not formatted according to the schema. The schema is and should be checked on the producer-side, not by the Kafka brokers themselves.**

The examples in this repository will show the different ways of producing to and consuming from the topic.

## Kafka REST Proxy
The [Kafka REST Proxy](https://docs.confluent.io/current/kafka-rest/index.html) provides a REST interface to produce and consume messages. The REST Proxy is a per-project instance. The examples produce and consume via a shell with [curl](https://curl.haxx.se/), but any command line HTTP client should be be sufficient.

For producing to and consuming from the project's topic, see the documentation for see [Kafka REST Proxy Avro](kafka-rest-proxy-avro.md).

## Confluent Python client
A Python interface is provided by the [Confluent Python client](https://github.com/confluentinc/confluent-kafka-python). The examples use Python 3, so please make sure you have a Python 3 environment available, e.g. through [Anaconda](https://www.anaconda.com/) or [virtualenv](https://virtualenv.pypa.io/en/latest/).

Before using any of the provided Python scripts, please install the required packages in the `requirements.txt` file with `pip`:

```
> pip install -r requirements.txt
Collecting confluent-kafka==0.11.6 (from -r requirements.txt (line 1))
  Using cached https://files.pythonhosted.org/packages/c7/51/acc0e56207e03ff4aef8dbcd93ce556fc9cb282e7493203024e9ddee9861/confluent_kafka-0.11.6-cp37-cp37m-macosx_10_6_intel.whl
Collecting avro-python3==1.8.2 (from -r requirements.txt (line 2))
Installing collected packages: confluent-kafka, avro-python3
Successfully installed avro-python3-1.8.2 confluent-kafka-0.11.6
```

### Producer
Messages produced to the schema topic are formatted as Avro records. Avro records have a fixed schema, which can be found in [`schema.avsc`](schema.avsc).

The [Kafka Avro producer script](kafka-producer-avro.py) will load the schema from this file first, and automatically serialise messages are according to the schema.

The producer requires the username and password for the project user when instantiating a Producer object, in the `sasl.username` and `sasl.password` fields, respectively. The number of cluster nodes for bootstrapping can be set with the `num_cluster_nodes` variable.

The topic name in `produce()` should be set to `${PROJECT_NAME}-schema`, and the schema should be specified with the `value_schema` parameter. The script will produce the same message to the topic indefinitely, with an exit (and producer flush) triggered on `Ctrl+C`.

### Consumer
The consumer script can be found [here](kafka-consumer-avro.py). As in the previous example, you will need to change the username and password to your project's username and password, and adapt the number of cluster nodes to the size of your cluster. The consumer requires the schema registry's URL.

**Please be aware that offsets are not automatically committed** (`enable.auto.commit` is set to `'false'`). In general, offsets should only be commited when a message has been processed successfully. Otherwise, a message may fail processing without any possibility of recovery because the offsets are past that message already.

The topic name should be set to `${PROJECT_NAME}-schema` in the `subscribe()` method. The consumer will poll for messages indefinitely and print them. The consumer can be stopped with `Ctrl+C`, where the `close()` will take care of committing the latest offsets.

## Kafka console producer and consumer
The Kafka console producer and consumer example can be found [here](kafka-console-schema.sh). Requirements are as follows:

1. An installed Confluent Platform tool set, as explained [here](https://docs.confluent.io/current/quickstart/ce-quickstart.html#ce-quickstart). Please make sure the `bin/` folder of the tool set is added to the path, so that the relevant commands can be found.
1. The `PROJECT_NAME` and `PROJECT_PASSWORD` need to be set, corresponding to the name and password of your project, respectively.

In addition, the script needs the URL of the Schema Registry in the `SCHEMA_REGISTRY_URL` variable in the script. The producer and consumer will use the registry to serialise and deserialise messages. They will throw an error if a message does not conform to the schema.

In addition, the scripts can be customised according to the number of brokers in your cluster by changing the `NUM_BROKERS` variable. The `URL_SUFFIX` can be changed depending on the node and cluster names.

The script will generate the list of Kafka brokers dynamically from the function `broker_list()`, and write the project username and password to a properties file that the producer and consumer will use to authenticate.

The script will write a JSON message to disk, and load the schema in the `SCHEMA` variable. To produce and consume with the Avro producers and consumers, we add the Schema Registry URL as a property to both with `--property schema.registry.url=${SCHEMA_REGISTRY_URL}`

## `kafkacat`
Another tool to interface with Kafka from the command line is [kafkacat](https://github.com/edenhill/kafkacat). `kafkacat` allows for console production and consumption, as well as inspecting metadata such as offsets and partitions.

An overview of some of the functionality can be found [here](kafkacat.sh). The script requires the project name and project password in the `PROJECT_NAME` and `PROJECT_PASSWORD` variables. Other variables can be modified as outlined in the Kafka console producer and consumer example.

For more information, please see the documentation from [kafkacat](https://github.com/edenhill/kafkacat) and [Confluent](https://docs.confluent.io/current/app-development/kafkacat-usage.html).
