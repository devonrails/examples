# Producer/consumer example for Kafka REST Proxy (Avro)
Avro messages can be produced and consumed using the Kafka REST Proxy.
Below is a number of shell commands (using curl) for creating a consumer, 
subscribing it to a topic, producing a message, and consuming it through REST
Proxy. The consumer is deleted afterwards.

## Requirements
Two environment variables need to be set, with which you will authenticate to
the REST Proxy: `PROJECT_NAME` and `PROJECT_PASSWORD`. Export these in your
favourite shell first:

```sh
> export PROJECT_NAME=your-project-name
> export PROJECT_PASSWORD=your-project-password
```

In addition, you will need to download and install the [jq](https://stedolan.github.io/jq/)
utility to pretty-print JSON returned by the REST Proxy. This is optional: 
if you do not want to use jq, please remove it from the commands below
yourself.

## Create a consumer in a consumer group
The response will show the name of the consumer instance and the URI with 
which we can communicate with the consumer:

```sh
> curl -v -X POST \
    -H "Content-Type: application/vnd.kafka.v2+json" \
    -H "Accept: application/vnd.kafka.v2+json" \
    -u $PROJECT_NAME:$PROJECT_PASSWORD \
    --data '{"name": "test-consumer", "format": "avro", "auto.offset.reset": "earliest"}' \
    https://$PROJECT_NAME-kafka-rest.green-village.sda-projects.nl/consumers/test-consumer-group | jq
```

**NOTE**: due to limitations of the REST proxy, it will return an HTTP URI by 
default, and not an HTTPS URI!

## Subscribe the consumer to the project topic
The URL in the last line is taken from the base URI in the response of the 
command above, with http changed into https.

Running the command below should give you a HTTP 204 response (No Content) to 
signal successful subscription:

```sh
> curl -v -X POST \
    -H "Content-Type: application/vnd.kafka.v2+json" \
    -u $PROJECT_NAME:$PROJECT_PASSWORD \
    --data '{"topics": ["'"${PROJECT_NAME}"'-schema"]}' \
    https://$PROJECT_NAME-kafka-rest.green-village.sda-projects.nl/consumers/test-consumer-group/instances/test-consumer/subscription
```

**NOTE**: the REST Proxy does not check whether the topic exists, so 
double-check it is correct.

## Produce a message to the schema topic
A schema has to be specified when producing messages with Kafka REST Proxy.
The schema can be referenced either as an integer identifier called `value_schema_id`
or as a string called `value_schema`. Both can be obtained from the Schema Registry:

```sh
export SCHEMA_ID=$(curl -s https://schema-registry.green-village.sda-projects.nl/subjects/$PROJECT_NAME-schema-value/versions/latest | jq .id)
export SCHEMA=$(curl -s https://schema-registry.green-village.sda-projects.nl/subjects/$PROJECT_NAME-schema-value/versions/latest | jq .schema)
```

First, write an example JSON message to a file called `message.json`:

```sh
echo '{
  "value_schema_id": '$SCHEMA_ID',
  "records": [
    {
      "value": {
        "data": {
          "timestamp": '$(date +%s)'000,
          "project_id": "'$PROJECT_NAME'",
          "device_id": "device1",
          "values": [
            {
              "description": "temperature",
              "name": "temperature",
              "type": "INT",
              "unit": "degrees",
              "value": {
                "int": 42
              }
            }
          ]
        },
        "metadata": {
          "description": "test device",
          "location": null,
          "latitude": null,
          "longitude": null,
          "altitude": null,
          "type": null,
          "manufacturer": null,
          "placement_timestamp": null,
          "project_id": "'$PROJECT_NAME'",
          "device_id": "device1",
          "serial": null
        }
      }
    }
  ]
}' > message.json
```

The command below will send the data to the REST Proxy. The response should
show you the offset and partition of the message:

```sh
> curl -v -X POST \
    -H "Content-Type: application/vnd.kafka.avro.v2+json" \
    -H "Accept: application/vnd.kafka.v2+json" \
    -u $PROJECT_NAME:$PROJECT_PASSWORD \
    --data @message.json \
    https://$PROJECT_NAME-kafka-rest.green-village.sda-projects.nl/topics/$PROJECT_NAME-schema | jq
```

## Produce a wrong message to the topic
Because we serialize our data into Avro according to a *schema*, the REST 
Proxy will check the message against that in the Schema Registry. If the
message does not conform to the schema, it will raise an error and not send
the data to the topic.

Let's create a message that is not compliant with the schema. In the example
below, we have removed the required field `description` from the metadata:

```sh
echo '{
  "value_schema_id": '$SCHEMA_ID',
  "records": [
    {
      "value": {
        "data": {
          "timestamp": '$(date +%s)'000,
          "project_id": "'$PROJECT_NAME'",
          "device_id": "device1",
          "values": [
            {
              "description": "temperature",
              "name": "temperature",
              "type": "INT",
              "unit": "degrees",
              "value": {
                "int": 42
              }
            }
          ]
        },
        "metadata": {
          "location": null,
          "latitude": null,
          "longitude": null,
          "altitude": null,
          "type": null,
          "manufacturer": null,
          "placement_timestamp": null,
          "project_id": "'$PROJECT_NAME'",
          "device_id": "device1",
          "serial": null
        }
      }
    }
  ]
}' > bad-message.json
```

Now try to produce the message:

```sh
> curl -v -X POST \
    -H "Content-Type: application/vnd.kafka.avro.v2+json" \
    -H "Accept: application/vnd.kafka.v2+json" \
    -u $PROJECT_NAME:$PROJECT_PASSWORD \
    --data @bad-message.json \
    https://$PROJECT_NAME-kafka-rest.green-village.sda-projects.nl/topics/$PROJECT_NAME-schema | jq
```

This will return the following error:

```json
{
  "error_code": 42203,
  "message": "Conversion of JSON to Avro failed: Failed to convert JSON to Avro: Expected field name not found: description"
}
```

The REST Proxy (through the schema registry) will tell us what was wrong with our message, so we can fix the mistake.

## Consume data from the topic
We can consume from the schema topic using the base URI returned by the first
command, with `http` changed into `https`:

```sh
> curl -v -X GET \
    -H "Accept: application/vnd.kafka.avro.v2+json" \
    -u $PROJECT_NAME:$PROJECT_PASSWORD \
    https://$PROJECT_NAME-kafka-rest.green-village.sda-projects.nl/consumers/test-consumer-group/instances/test-consumer/records | jq
```

## Delete the consumer from the consumer group
We should clean up the consumer by deleting it from the consumer group:

```sh
> curl -v -X DELETE -H "Content-Type: application/vnd.kafka.v2+json" \
    -u $PROJECT_NAME:$PROJECT_PASSWORD \
    https://$PROJECT_NAME-kafka-rest.green-village.sda-projects.nl/consumers/test-consumer-group/instances/test-consumer
```

You should see a 204 response (No Content) to signal successful deletion.

## More information
More information can be found in REST Proxy's [API documentation](https://docs.confluent.io/current/kafka-rest/api.html).
