#!/bin/bash
set -ex

# Make sure you have kafkacat installed and it is available in your PATH
# See: https://github.com/edenhill/kafkacat

NUM_BROKERS=1  # Should be set to the number of brokers (worker nodes)
URL_SUFFIX="-worker-green-village.green-village.sda-projects.nl"  # The worker URL part of the cluster
PROJECT_NAME="your project name"
PROJECT_PASSWORD="your project password"
SCHEMA_REGISTRY_URL="https://schema-registry.green-village.sda-projects.nl"

# This function generates a list of brokers that we can use with the Kafka command line clients

function broker_list() {
    for index in $(seq 1 ${NUM_BROKERS});
    do
        local node_index=$(printf "%02d" ${index})
        let port=31090+${index}-1
        local broker_url="${node_index}${URL_SUFFIX}:${port}"

        if [ -z "${brokers}" ];
        then
            local brokers=${broker_url};
        else
            local brokers="${brokers},${broker_url}"
        fi
    done

    echo ${brokers}
}

BROKERS=$(broker_list)

# Create an Avro message

echo -n '{
    "data": {
      "timestamp": '$(date +%s)'000,
      "project_id": "'$PROJECT_NAME'",
      "device_id": "device1",
      "values": [
        {
          "description": "temperature",
          "name": "temperature",
          "type": "INT",
          "unit": "degrees",
          "value": {
            "int": 42
          }
        }
      ]
    },
    "metadata": {
      "description": "test device",
      "location": null,
      "latitude": null,
      "longitude": null,
      "altitude": null,
      "type": null,
      "manufacturer": null,
      "placement_timestamp": null,
      "project_id": "'$PROJECT_NAME'",
      "device_id": "device1",
      "serial": null
    }
}' > message.json

# Load the schema

SCHEMA=$(cat schema.avsc | jq -rc)

# Produce a message with the schema and schema registry

cat message.json | jq -rc | kafka-avro-console-producer \
    --broker-list ${BROKERS} \
    --property value.schema="${SCHEMA}" \
    --property schema.registry.url=${SCHEMA_REGISTRY_URL} \
    --topic "${PROJECT_NAME}-schema" \
    --producer.config client.properties

# Consume messages

kafka-avro-console-consumer \
    --bootstrap-server ${BROKERS} \
    --topic "${PROJECT_NAME}-schema" \
    --from-beginning \
    --consumer.config client.properties \
    --property schema.registry.url=${SCHEMA_REGISTRY_URL}

# List Kafka metadata such as the online brokers, offsets and partitions

kafkacat -L \
  -b ${BROKERS} \
  -X security.protocol=SASL_PLAINTEXT \
  -X sasl.mechanism=PLAIN \
  -X sasl.username=${PROJECT_NAME} \
  -X sasl.password=${PROJECT_PASSWORD}

# Show the last message from the schema topic
# We are using the kafkacat Docker image, since not all precompiled versions support Schema Registry at the moment

docker run -t edenhill/kafkacat:20190711 \
  -C -t ${PROJECT_NAME}-schema -o -1 -p 0 -e \
  -s value=avro \
  -r ${SCHEMA_REGISTRY_URL} \
  -b ${BROKERS} \
  -X security.protocol=SASL_PLAINTEXT \
  -X sasl.mechanism=PLAIN \
  -X sasl.username=${PROJECT_NAME} \
  -X sasl.password=${PROJECT_PASSWORD}
