"""
This example script consumes Avro messages from the project's topic.
"""
import json
import logging
import sys

from confluent_kafka import KafkaError, KafkaException
from confluent_kafka.avro import AvroConsumer


def get_bootstrap_server(node_index):
    """
    Return a bootstrap server based on the index of the node in the cluster.
    """
    hostname_suffix = 'worker-green-village.green-village.sda-projects.nl'
    return f'{node_index + 1:02}-{hostname_suffix}:{31090 + node_index}'


if __name__ == '__main__':
    num_cluster_nodes = 3  # change this number to the number of nodes (brokers) in your cluster
    bootstrap_servers = [get_bootstrap_server(i) for i in range(num_cluster_nodes)]

    consumer = AvroConsumer({
        "bootstrap.servers": ','.join(bootstrap_servers),
        'schema.registry.url': 'https://schema-registry.green-village.sda-projects.nl:443/',
        'group.id': 'test-consumer-group',  # the consumer must have a consumer group ID
        "security.protocol": "SASL_PLAINTEXT",  # authentication options
        "sasl.mechanisms": "PLAIN",
        "sasl.username": "test",
        "sasl.password": "1234",
        'auto.offset.reset': 'earliest',  # start from earliest offset
        'enable.auto.commit': 'false',  # only commit when we have processed a message
    })

    # Subscribe to topics

    consumer.subscribe(['test-schema'])

    # Read messages from Kafka, print to stdout

    try:
        while True:
            msg = consumer.poll(timeout=1.0)
            if msg is None:
                continue  # retry if no message received

            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    # We can reach the end of the partition, but want to keep
                    # listening for more messages
                    print('Reached end of partition (partition EOF)')
                    continue

                # Raise Python exception if any other error
                
                raise KafkaException(msg.error())
            else:
                print(f'Message from topic {msg.topic()} [{msg.partition()}] at offset {msg.offset()}: key={msg.key()}, value=${msg.value()}')

                # Commit the offset: we are done processing this message

                consumer.commit()
    except KeyboardInterrupt:
        print('Aborted by user', file=sys.stderr)
    finally:
        # Close down consumer to commit final offsets

        consumer.close()
