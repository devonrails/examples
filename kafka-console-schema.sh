#!/bin/bash

# Make sure you have the Confluent Platform utilities in your path
# See: https://docs.confluent.io/current/quickstart/ce-quickstart.html#ce-quickstart

NUM_BROKERS=5  # Should be set to the number of brokers (worker nodes)
URL_SUFFIX="-worker-green-village.green-village.sda-projects.nl"  # The worker URL part of the cluster
PROJECT_NAME="your project name"
PROJECT_PASSWORD="your project password"
SCHEMA_REGISTRY_URL="https://schema-registry.green-village.sda-projects.nl"

# This function generates a list of brokers that we can use with the Kafka command line clients

function broker_list() {
    for index in $(seq 1 ${NUM_BROKERS});
    do
        local node_index=$(printf "%02d" ${index})
        let port=31090+${index}-1
        local broker_url="${node_index}${URL_SUFFIX}:${port}"

        if [ -z "${brokers}" ];
        then
            local brokers=${broker_url};
        else
            local brokers="${brokers},${broker_url}"
        fi
    done

    echo ${brokers}
}

BROKERS=$(broker_list)

# Store client properties

echo """sasl.mechanism=PLAIN
security.protocol=SASL_PLAINTEXT
sasl.jaas.config=org.apache.kafka.common.security.plain.PlainLoginModule required username="$PROJECT_NAME" password="$PROJECT_PASSWORD";""" > client.properties

# Create an Avro message

echo -n '{
    "data": {
      "timestamp": '$(date +%s)'000,
      "project_id": "'$PROJECT_NAME'",
      "device_id": "device1",
      "values": [
        {
          "description": "temperature",
          "name": "temperature",
          "type": "INT",
          "unit": "degrees",
          "value": {
            "int": 42
          }
        }
      ]
    },
    "metadata": {
      "description": "test device",
      "location": null,
      "latitude": null,
      "longitude": null,
      "altitude": null,
      "type": null,
      "manufacturer": null,
      "placement_timestamp": null,
      "project_id": "'$PROJECT_NAME'",
      "device_id": "device1",
      "serial": null
    }
}' > message.json

# Load the schema

SCHEMA=$(cat schema.avsc | jq -rc)

# Produce a message with the schema and schema registry

cat message.json | jq -rc | kafka-avro-console-producer \
    --broker-list ${BROKERS} \
    --property value.schema="${SCHEMA}" \
    --property schema.registry.url=${SCHEMA_REGISTRY_URL} \
    --topic "${PROJECT_NAME}-schema" \
    --producer.config client.properties

# Consume messages

kafka-avro-console-consumer \
    --bootstrap-server ${BROKERS} \
    --topic "${PROJECT_NAME}-schema" \
    --from-beginning \
    --consumer.config client.properties \
    --property schema.registry.url=${SCHEMA_REGISTRY_URL}
